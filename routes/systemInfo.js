const express = require('express');
const router = express.Router();

//
const si = require('systeminformation');

/*GET endpoint to return system information*/
router.get('/', (req, res, next) => {    
    Promise.all([
        si.system(),
        si.osInfo(),
        si.cpu(),
        si.diskLayout()
    ])
    .then(responses => {        
        res.send({
            //system
            manufacturer: responses[0].manufacturer,
            //os
            platform: responses[1].platform,
            distro: responses[1].distro,
            //cpu
            cpu: responses[2].brand,
            //disks
            disks: responses[3].map(disk => {
                return (({type, name, size}) => ({type, name, size}))(disk);
            })
        });
    })
    .catch(error => next(error));
});

module.exports = router;
